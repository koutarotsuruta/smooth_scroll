window.addEventListener('load',function(){
    var a = document.getElementsByTagName('a');
    var scrollSpeed = 0.2;
 
    for (var i = 0; i < a.length; i++) {
        a[i].addEventListener('click', function (e) {
            var c = [].indexOf.call(a, this);
            var href = a[c].getAttribute('href');
            if (href.match(/#/)) {
                e.preventDefault();
                var scrollPosition = window.pageYOffset + document.getElementById(href.substr(1)).getBoundingClientRect().top;
                var nowPosition = window.scrollY || window.scollTop || document.getElementsByTagName("html")[0].scrollTop;
                if (nowPosition < scrollPosition) {
                    var diff = scrollPosition - nowPosition;
                    for (var t = 0; t < diff; t++) {
                        (function (pram) {
                            setTimeout(function () {
                                var scrollTop = nowPosition + pram;
                                window.scrollTo(0, scrollTop);
                            }, pram * scrollSpeed);
                        })(t);
                    }
                } else {
                    var diff = nowPosition - scrollPosition;
                    for (var t = 0; t < diff; t++) {
                        (function (pram) {
                            setTimeout(function () {
                                var scrollTop = nowPosition - pram;
                                window.scrollTo(0, scrollTop);
                            }, pram * scrollSpeed);
                        })(t);
                    }
                }
            }
        });
    }
});
